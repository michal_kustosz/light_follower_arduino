#include <Servo.h>

Servo LeftServo;
Servo RightServo;

#define ServoLeftArgumentZero  88       //stop cond 88 is zero state !!!!!!!!!!!
#define ServoRightArgumentZero  94     //stop condition 94 is zero state !!!!!!

#define Button1  2                      //2 pin przyciski na robocie
#define Button2  4                     //4 pin przyciski na robocie

int ldrRight, ldrMidle, ldrLeft;       //global

//////////////////////////////////////////////////////////////////////////////////
//Servo Right Arg if > 94 then backward
// if Servo Left Arg < 88 then backward
//*************************************
///Servo Right Arg if < 94 then forward
// if Servo Left Arg > 88 then forward
//////////////////////////////////////////////////////////////////////////////////backward
void backward(int Velocity) {  //velocity is an argument

 RightServo.write(ServoRightArgumentZero + Velocity );
 LeftServo.write(ServoLeftArgumentZero - Velocity );
}
//////////////////////////////////////////////////////////////////////////////////forward
void forward(int Velocity) {   //velocity is an argument

  RightServo.write(ServoRightArgumentZero - Velocity );
  LeftServo.write(ServoLeftArgumentZero + Velocity );
}
/////////////////////////////////////////////////////////////////////////////////turnLeft
void turnLeft(int Velocity) {

  RightServo.write(ServoRightArgumentZero - Velocity ); 
  LeftServo.write(ServoLeftArgumentZero - Velocity );
}
/////////////////////////////////////////////////////////////////////////////////turnRight
void turnRight(int Velocity) {

  RightServo.write(ServoRightArgumentZero + Velocity );
  LeftServo.write(ServoLeftArgumentZero + Velocity );
}
//////////////////////////////////////////////////////////////////////////////////Stop
void Stop() {
  RightServo.write(ServoRightArgumentZero);      // from 20 to 150 !!!!RIGHT 94 STOP
  LeftServo.write(ServoLeftArgumentZero);        //!!!!!!!!!!88 LEFT STOP !!!!!!!!!!
}
///////////////////////////////////////////////////////////////////////////////Navigation
void Navig(int ldrLeft, int ldrMidle, int ldrRight, int velocity, int timeMiliSec){    //przekazuje argument timeMiliSec ale nic z nim specjalnego nie robi
   
    if(ldrLeft > ldrRight && ldrLeft > ldrMidle){
      digitalWrite(7,HIGH);  
      digitalWrite(12,LOW);                                 
      digitalWrite(13,LOW);
      turnLeft(velocity);
    }
    
    else if(ldrRight > ldrLeft && ldrRight > ldrMidle){    
      digitalWrite(7,LOW);  
      digitalWrite(12,HIGH);                                 
      digitalWrite(13,LOW);
      turnRight(velocity);
    }
    
    else if( ldrMidle > ldrRight || ldrMidle > ldrLeft ){
      forward(velocity);
      digitalWrite(7,LOW);
      digitalWrite(12,LOW);                                 
      digitalWrite(13,HIGH);
    } 
    
}///////////////////////////////////////////////////////////////////////////////////////////////////////////End Navig
  
void setup() {
  Serial.begin(9600);
  
  RightServo.attach(5);
  LeftServo.attach(6);
  
  RightServo.write(ServoRightArgumentZero);      // set velocity = 0 each servo
  LeftServo.write(ServoLeftArgumentZero);        // set velocity = 0 each servo
  
  pinMode(Button1, INPUT);
  pinMode(Button2, INPUT);
  
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  
   pinMode(12,OUTPUT);                            //dioda Orange
   pinMode(13,OUTPUT);                            //dioda Red
   pinMode(7,OUTPUT);
  
        //Calibration();                          //call calibration function it's using a global variables
}       //end setup

void loop() {
      
    ldrRight = analogRead(A0);                   //right
    ldrMidle = analogRead(A1);                   //midlle
    ldrLeft = analogRead(A2);                    //left
    
    int velocity = 30;                          //argument of forward, turnL, etc. functions
    int timeMiliSec = 333;                      // it is parameter of the function Navig()
   
    Navig(ldrLeft, ldrMidle, ldrRight, velocity, timeMiliSec);
                
}//end loop


